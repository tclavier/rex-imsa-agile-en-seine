--- 
author: Thomas Clavier & Stéphane Hecquet
title: Retour d'expérience sur un train SAFe pas comme les autres
---

## {data-background-image="./includes/agile-en-seine-titre.png"}

## {data-background-image="./includes/agile-en-seine-sponsors.png"}

# Présentation

## Photo du sommet à atteindre

![](./includes/photo-sommet.jpg)

:::notes
Challenge illuster cette présentation uniquement avec nos photos de vacances

On commence par un objectif : un sommet à atteindre.
:::

##

<table>
<tr>
    <td align=center>
        ![](./includes/stephane-hecquet.jpg){height=500px}<br/>
        Stéphane Hecquet
    </td>
    <td align=center>
        ![](./includes/thomas-clavier.jpg){height=500px}<br/>
        Thomas Clavier
    </td>
</table>

## iMSA {.small}

:::: {.columns}
::: {.column width="50%"}
* IMSA développe et exploite le logiciel informatique de la MSA et de ses partenaires
* Couverture de l'ensemble du périmètre de la protection sociale pour le Régime Obligatoire de l’ensemble de la population agricole et des ayants droit : exploitants, salariés (d’exploitations, d’entreprises, de coopératives et d’organismes). 5,6 millions de bénéficiaires
* Spécificité pour la gestion des Régimes Complémentaires
* Gestion Pour Comptes de l'activité de partenaires (Assurance Complémentaire, Autres partenaires qui souhaitent utiliser le logiciel)
* Produit s'adressant aux agents et aux adhérents (MSA et partenaires)
:::
::: {.column width="50%"}
![](./includes/logo-imsa.png)
:::
::::

##

:::: {.columns}
::: {.column width="50%"}
![](./includes/logo-imsa.png)
:::
::: {.column width="50%"}
* informatique de la MSA et de ses partenaires
* Guichet unique pour le monde agricole : 5,6 millions de bénéficiaires
:::
::::

:::notes

* IMSA développe et exploite le logiciel informatique de la MSA et de ses partenaires
* Couverture de l'ensemble du périmètre de la protection sociale pour le Régime Obligatoire de l’ensemble de la population agricole et des ayants droit : exploitants, salariés (d’exploitations, d’entreprises, de coopératives et d’organismes). 5,6 millions de bénéficiaires
* Spécificité pour la gestion des Régimes Complémentaires
* Gestion Pour Comptes de l'activité de partenaires (Assurance Complémentaire, Autres partenaires qui souhaitent utiliser le logiciel)
* Produit s'adressant aux agents et aux adhérents (MSA et partenaires)
:::

## Le sujet

La direction d'IMSA a décidé pour répondre aux enjeux d'efficience, de pertinence de construction du logiciel par la priorisation de la valeur de lancer l'utilisation du framework SAFe

Après un premier train pilote sur l'expérience Digitale, notre Direction SI Métier a décidé de lancer un train sur un volet "Métier" incluant un produit déjà existant et en perpétuelle évolution et devant intégrer les enjeux législatifs à délais contraints.

Notre intervention de ce jour à pour objectif de de vous partager notre retour d'expérience de la mise en place de ce train particulier

#  Contexte

## Photo guide/montagne Valeur: accompagnement/aide

![](./includes/photo-guide.jpg)

## Contexte projet

* Mise en oeuvre d'un Système d'Information dédié au Déclaratif (DSN).
* Programme déjà lancé, avec des fondations et ne pouvant s'arrêter pour un lancement
* Des pratiques agiles et une acculturation anticipée
* Un mode entièrement distanciel qui augmente encore le challenge de cette transformation

## Contexte projet

* Le système DSN.
* déjà en cours
* essai d’agilité, acculturation mais pas d’accompagnement : on appliquait des éléments à la lettre.

# Stratégie

## {data-background-image="./includes/safe-roadmap.png"}

## Dans la vraie vie

:::: {.columns}
::: {.column width="50%"}
* Bascule vers une organisation produit
* System Team == Qualité
* Avancer par petit pas
* En distanciel
:::
::: {.column width="50%"}
![](./includes/photo-relive.jpg)
:::
::::

::: notes
Stéphane : démarrage en mode lancé, itératif/transformation progressive. On a choisi une solution dans une contexte où on ne pouvait pas s’arrêter. **Contexte legacy.**

c’est différent de ce qui avait déjà été fait chez IMSA dans des contexte plutôt « digital »
:::

## Accompagné par des pros

:::: {.columns}
::: {.column width="50%"}
![](./includes/logo-inetum.png){height=100px}

* Coaching de la direction
* Gestion produit / backlog
* Mise en place des rituels

:::
::: {.column width="50%"}
![](./includes/logo-azae.svg){height=100px}

* Software craftsmanship
* Excellence technique

:::
::::

## Loie de conway

> les organisations qui conçoivent des systèmes [...] tendent inévitablement à produire des designs qui sont des copies de la structure de communication de leur organisation.

## Techniquement

![](./includes/archi.png){height=500px}

# L'histoire

## Le départ

## {data-background-image="./includes/photo-depart.jpg"}

:::notes
Départ (1800m) pour un bivouac à l'Ibon Blanco de Literola (2700m)
:::

## Printemps 2020

Création Guiding Coalition (Dir de transfo, Sponsor, PM, RTE, SAE, puis BO)

* Trouver le modèle d’orga
* Piloter la transformation


## Été 2020

## {data-background-image="./includes/photo-en-marche.jpg"}

## Juin 2020
* Lancement d’une organisation en "feature team"
* Présentation de l'organisation et démarrage (Kick Off)
* Sensibilisation à l'agilité (base et perfectionnement)
* Gommage d’une certaine hétérogénéité des connaissances Agiles

## Bilan de saison

* Acceptation d’une nouvelle organisation suite à de nombreuses réorganisations.
* Convaincu de l’importance de cette façon de faire. 
* Nouvelle approche par petites fonctionnalités (US) « produit ».

## Photo à voir: photo groupe qui marche ensemble

![](./includes/photo-groupe.jpg)
 

## Octobre 2020

## {data-background-image="./includes/voie-acces.svg"}

## Photo rando en cours: exemple/photo d'un chemin qui monte. Valeur Humilité

## Octobre 2020

Mini PI planning sur 1j à distance précédé de la formation Safe 4 Teams. 

![](./includes/dessin-pir7.jpg){height=400px}

## Bilan de saison

* Les équipes sont installées
* Cérémonies d'équipe : ✅ sauf démo d'itération
* Cérémonies du train : ✅
* Mise en place d’OKR suivi en GT.
* Ritualisation des entrainements : 0,5j/semaine/équipe.
* Craft Day #1 : SIDL + iMSA.
 
## Bilan PI

* Atteinte des objectifs PI/BV et prédictibilité : 80%
* Vélocité/US : 2/3.
* Les éléments développés sont majoritairement mis en production.
* + X ETP

## Croissance

* ↗️  nouveaux équipiers avec toujours l’accompagnement et la formation agile et SAFe.
* ⚠️  ne pas déstabiliser les équipes.

## Janvier 2021

PIP sur 2J en distanciel

![](./includes/photo-pipr8.png){height=400px}

## Points notables

* Forte implication du BO même si c'est douloureux
* Acculturation à l’exercice
* Un second PM est intégré issu du métier

## Bilan de saison

* Définition d’OKR avec contribution des teams.
* Ritualisation des cérémonies tri-amigo et début du BDD
* 1ères démos d’itération (entre équipes puis avec parties prenantes)
* Craft Day #2 : devient le craft day d’iMSA.

## {data-background-image="./includes/photo-bivouac.jpg"}

## Bilan intermédiaire

* 🙂 Les équipes sont montées en autonomies
* 🙂 Les tech lead sont accompagnants
* 🙂 Le coaching de base n’est plus nécessaire et se focalise sur les points difficiles (nouvelle team, OPS)
* 🙂 les PO sont en maîtrise de la Backlog et en négo avecs les PM au quotidien
* 😞 Les scrums Masters sont encore sur plusieurs équipes et sont en difficultés

## Bilan de PI 

* Atteinte des objectifs PI/BV et prédictibilité : 90%
* Vélocité/US : 3/4.
* Une partie seulement du produit est mis en production.
* + X ETP

## Photo  Rythme soutenable (groupe qui marche)

## Mai 2021

PIP sur 2j

## 14h07 {data-background-image="./includes/photo-avant-orage.jpg"}

## 14h26 {data-background-image="./includes/photo-apres-orage.jpg"}

## Procedure SAFe

* Une très grosse priorité dans 1 team.
* Le BO décide de focaliser le train dessus.
* Le second jour, toutes les teams se mobilisent.

## Bilan de saison

* La mécanique de SAFe nous a sauvé
* Une system team opérationnelle
* Encore des niveaux hétérogènes de pratiques entre le delivery et l’amélioration des pratiques
* Les OKR c'est bien mangez-en !

## Bilan de PI 

* Atteinte des objectifs PI/BV et prédictibilité : 81,5%
* Vélocité/US : 3/4.
* 60% est mis en production
* + X ETP

## Photo changement de chemin : Sommet vue sous un autre angle, capaciter à prendre du recul et accepter le changement. Notion IA, actions pour faire mieux...*

## Septembre 2021

PIP sur 2j mais repoussé.

## Photo du sommet mais on y est pas encore / photo d'un sommet plus grand

# Bilan

## Les difficultés

* Le delivery une fois par PI
* La notion de petites fonctionnalités pour l’utilisateur, mais avec des progrès grâce les ateliers métiers en amont aux démos
* Les exigences parfois à rappeler

## Photo pluie ou orage

## Les victoires

- prise du lead/automonie des développeurs mas avec maintien des tech lead par teams : teach lead accompagnateurs.
- l’ambiance, l’entraide, la polyvalence (on ne fait pas chacun à son tour en mode process mais ensemble pour atteindre les US)
- les démos
- une augmentation de la vitesse d’execution (vélocité) d'environ 2 % par PI, avec une augmentation continue de la taille du  train
- une  capacité d’adaptation rapide des teams pour répondre aux priorités (US basculent dans équipe mois chargée)

## {data-background-image="./includes/photo-panorama.jpg"}
Pic de Sacroux - 2676m

# Analogie {.small}

| En montagne | Dans l'agilité |
|-------------|----------------|
| Guide | coach |
| Groupe | ensemble et non esseulé |
| Humilité | au début on ne connais pas, on apprends et on continue à apprendre et à s’améliorer avec les autres |
| Rythme soutenable | on va régulièrement mais sûrement pour ne pas tomber en surchauffe et aller au bout, rythme identique pour tous |
| Se tromper de chemin et redescendre | retro et IA. on regarde ou on s’est trompé et on reprends de bon chemin |
| S’arrêter se retourner | prendre du recul, observer la transformation. OKR + démo de transfo (avec recul) |
| Il fait parfois moins beau | on sort l’équipement pour palier, on innove pour continuer, on se préserve des éléments |
| Entrainement avant, après et pendant | sensibilisation Agile, homogénéité, formation, s’exercer (craft) |
